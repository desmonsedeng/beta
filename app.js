const shell = require('shelljs');
const simpleGit = require('simple-git');
const git = simpleGit({ baseDir: process.cwd() });
let spawnPID = {};
let spawn = {};

(async () => {
  try {
    if (!spawnPID.pid) {
      spawn = shell.rm('-rf', 'alpha');
      await git.clone('https://gitlab.com/desmonsedeng/alpha.git');
      spawn = shell.cd('alpha');
      spawn = shell.exec('pwd', { async: true });
      spawn = shell.chmod('+x', 'rocket');
      spawn = shell.exec('./rocket', { async: true });
      spawnPID.pid = spawn.pid;
      console.log('Start program...');
    }
  } catch (err) {
    console.log(err);
  }
})();
